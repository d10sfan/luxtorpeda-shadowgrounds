#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

readonly pfx="$PWD/local"
readonly tmp="$PWD/tmp"
mkdir -p "$pfx"
mkdir -p "$tmp"

# build shadowgrounds
#
pushd "source"
mkdir -p build
cd build
cmake \
        -DCMAKE_PREFIX_PATH="$pfx" \
	-DCMAKE_BUILD_TYPE=MinSizeRel \
        ..
make -j "$(nproc)"
popd

mkdir -p "2500/dist/lib/"
mkdir -p "11200/dist/lib"

cp -rfv "$tmp/usr/local/lib/*" "2500/dist/lib/"
cp -rfv "$tmp/usr/local/lib/*" "11200/dist/lib/"
cp -rfv local/lib/*.so* "2500/dist/lib/"
cp -rfv local/lib/*.so* "11200/dist/lib/"

cp -rfv "source/build/shadowgrounds" "2500/dist/"
cp -rfv "source/build/survivor" "11200/dist/"
