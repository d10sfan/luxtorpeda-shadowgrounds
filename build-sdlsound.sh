#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

readonly pfx="$PWD/local"
readonly tmp="$PWD/tmp"
mkdir -p "$pfx"
mkdir -p "$tmp"

# build sdl sound
pushd "SDL_sound-1.0.3"
./configure
make -j "$(nproc)"
DESTDIR="$tmp" make install
popd
